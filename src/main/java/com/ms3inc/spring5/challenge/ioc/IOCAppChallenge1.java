package com.ms3inc.spring5.challenge.ioc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class IOCAppChallenge1 {

	public static void main(String[] args) {
//		 FileSystemXmlApplicationContext look for the beans-challenge.xml on the ROOT project directory.
    	ApplicationContext ctx = new FileSystemXmlApplicationContext("beans-challenge.xml");
    	
    	City city = (City) ctx.getBean("mycity");
    	
    	city.cityName();
    	
    	((FileSystemXmlApplicationContext) ctx).close();
	}

}
