package com.ms3inc.spring5.challenge.ioc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class IOCAppChallenge2 {

	public static void main(String[] args) {
    	
    	// ClassPathXmlApplicationContext finds for the beans-challenge.xml on src/main/resources directory.
    	ApplicationContext ctx = new ClassPathXmlApplicationContext("beans-challenge.xml");
    	
    	City city = (City) ctx.getBean("mycity");
    	
    	city.cityName();
    	
    	((ClassPathXmlApplicationContext) ctx).close();
	}

}
